package main

import (
	"fmt"
)

func intToBinary(v int) []rune {

	vBinaryStr := []rune(fmt.Sprintf("%b", v))
	length := len(vBinaryStr)
	if length > 32 {
		return nil
	}
	var vBinary []rune
	for i := 0 ; i < 32; i++ {
		vBinary = append(vBinary, 48)
	}
	for i, j := length-1, 0 ; i>= 0; i-- {
		vBinary[j] = vBinaryStr[i]
		j++
	}
	return vBinary
}

func hammingDistance(x int, y int) int {

	xBinary := intToBinary(x)
	yBinary := intToBinary(y)
	count := 0
	for i := range xBinary {
		if xBinary[i] != yBinary[i] {
			count++
		}
	}
	return count
}

func intToBinary2(v int, b int, bStr []string) []string {

	if v % b == 1 {
		bStr = append(bStr, "1")
	} else {
		bStr = append(bStr, "0")
	}

	if v == 1 || v == 0 {
		for i := len(bStr); i < 32; i ++ {
			bStr = append(bStr, "0")
		}
		fmt.Println(bStr)
		return bStr
	}

	return intToBinary2(v/b, b, bStr)
}

func intToBinary3(v int, b int, bStr *[]string) {

	if v % b == 1 {
		*bStr = append(*bStr, "1")
	} else {
		*bStr = append(*bStr, "0")
	}

	if v == 1 || v == 0 {
		for i := len(*bStr); i < 32; i ++ {
			*bStr = append(*bStr, "0")
		}
		fmt.Println(*bStr)
		return
	}

	intToBinary3(v/b, b, bStr)
}

func hammingDistance2(x int, y int) int {
	var xBinary, yBinary []string
	xBinary = intToBinary2(x, 2, nil)
	yBinary = intToBinary2(y, 2, nil)

	counts := 0
	for i := range xBinary {
		if xBinary[i] != yBinary[i] {
			counts++
		}
	}
	return counts

}


func main() {
	//hammingDistance(1, 4)
	hammingDistance2(0, 1)


}
