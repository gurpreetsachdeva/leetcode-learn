package main

func findWords(words []string) []string {

    alphabets := []string{"qwertyuiop", "asdfghjkl", "zxcvbnm"}
    var output []string
    for _, word := range words {
        for _, alphabet := range alphabets {
            count := 0
            for i := range word {
                for j := range alphabet {
                    if word[i] == alphabet[j] || (word[i]+32) == alphabet[j] {
                        count++
                    }
                }
            }
            if count == len(word) {
                output = append(output, word)
                break
            }
        }
    }
    return output
}

func findWords2(words []string) []string {

    alphabets := []map[byte]bool{
        {
            'q': true, 'w': true, 'e': true, 'r': true, 't': true, 'y': true, 'u': true, 'i': true, 'o': true, 'p': true,
        },
        {
            'a': true, 's': true, 'd': true, 'f': true, 'g': true, 'h': true, 'j': true, 'k': true, 'l': true,
        },
        {
            'z': true, 'x': true, 'c': true, 'v': true, 'b': true, 'n': true, 'm': true,
        },
    }
    var output []string
    for _, word := range words {
        for _, alphabet := range alphabets {
            count := 0
            for i := range word {
                if !alphabet[word[i]] && !alphabet[word[i]+32] {
                    break
                }
                count++
            }
            if count == len(word) {
                output = append(output, word)
                break
            }
        }
    }
    return output
}

func main() {
    words := []string{"Hello", "Alaska", "Dad", "Peace"}
    findWords2(words)
}
