package main

import (
	"sort"
	"fmt"
)

func sortArrayByParity(A []int) []int {
	evens := make([]int, 0)
	odds := make([]int, 0)
	sort.Ints(A)
	for i := range A {
		if A[i] % 2 == 0 {
			evens = append(evens, A[i])
		} else {
			odds = append(odds, A[i])
		}
	}
	return append(evens, odds...)
}

func main() {
	A := []int{3, 1, 4, 2}
	fmt.Println(sortArrayByParity(A))
}
