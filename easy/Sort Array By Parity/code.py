

def sort_array_by_parity(A):
    A.sort()
    return list(filter(lambda x: x % 2 == 0, A)) + list(filter(lambda x: x % 2, A))


def main():
    A = [3, 1, 2, 4]
    print(sort_array_by_parity(A))


main()
