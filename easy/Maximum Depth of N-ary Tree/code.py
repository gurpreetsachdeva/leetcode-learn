# -*- coding: utf-8 -*-
""" By Daath """


# Definition for a Node.
class Node(object):
    def __init__(self, val, children=None):
        self.val = val
        self.children = children or []


def init_nary_tree(tree_dict):

    if tree_dict == {}:
        return None

    node = Node("")
    for k, v in tree_dict.iteritems():
        if k == "val":
            node.val = v
        if k == "children":
            node.children = []
            for j in v:
                node.children.append(init_nary_tree(j))

    return node


def get_max_depth(root):
    if root is None:
        return 0
    if not root.children:
        return 1
    sub_max_depth = 0
    # compare each sub tree depth
    for i in root.children:
        sub_depth = get_max_depth(i)
        if sub_max_depth < sub_depth:
            sub_max_depth = sub_depth
    return sub_max_depth + 1


class Solution(object):
    def maxDepth(self, root):
        """
        :type root: Node
        :rtype: int
        """
        if root is None:
            return 0
        if not root.children:
            return 1
        sub_max_depth = 0
        for i in root.children:
            sub_depth = self.maxDepth(i)
            if sub_max_depth < sub_depth:
                sub_max_depth = sub_depth
        return sub_max_depth + 1


def main():
    tree_dict = {
        "$id": "1",
        "children": [
            {
                "$id": "2",
                "children": [
                    {
                        "$id": "5",
                        "children": [
                        ],
                        "val": 5
                    },
                    {
                        "$id": "6",
                        "children": [
                        ],
                        "val":6
                    }
                ],
                "val": 3
            },
            {
                "$id": "3",
                "children": [],
                "val": 2
            },
            {
                "$id": "4",
                "children": [],
                "val": 4
            }
        ],
        "val": 1
    }
    root = init_nary_tree(tree_dict)

    print(get_max_depth(root))

    solution = Solution()
    depth = solution.maxDepth(root)
    print(depth)


main()

