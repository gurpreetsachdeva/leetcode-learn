package main

import (
    "fmt"
    "time"
)

func spiltNumber(num int) []int {
    divisor := 10000
    canInsert := false
    var spiltNum []int
    for divisor > 0 {
        div := num / divisor
        mod := num % divisor
        if div != 0 {
            canInsert = true
        }
        if canInsert {
            spiltNum = append(spiltNum, div)
            num = mod
        }
        divisor /= 10
    }

    return spiltNum
}

func spiltNumber2(num int, divisor int, spiltNum *[]int) {
    if divisor == 0 {
        return
    }
    div := num / divisor
    mod := num % divisor
    if div != 0 || mod == 0 {
        *spiltNum = append(*spiltNum, div)
    }
    spiltNumber2(mod, divisor/10, spiltNum)
}

func selfDividingNumbers(left int, right int) []int {

    var selfDiv []int
    var spiltNum []int
    for i := left; i <= right; i++ {
        isInsert := false
        spiltNum = spiltNumber(i)
        //spiltNum = nil
        //spiltNumber2(i, 10000, &spiltNum)
        for _, v := range spiltNum {
            if v != 0 && i % v == 0 {
                isInsert = true
            } else {
                isInsert = false
                break
            }
        }
        if isInsert {
            selfDiv = append(selfDiv, i)
        }
    }
    return selfDiv
}

// reference to other
func selfDividingNumbers2(left int, right int) []int {
    var selfDiv []int
    for i := left; i <= right; i++ {
        j := i
        for j != 0 && (j%10) != 0 && i%(j%10) == 0 {
            j /= 10
        }
        if j == 0 {
            selfDiv = append(selfDiv, i)
        }
    }
    return selfDiv

}


func main() {
    start := time.Now().Nanosecond()
    fmt.Println(selfDividingNumbers(1, 22))
    end := time.Now().Nanosecond()
    fmt.Println("time: ", (end - start)/1000)

    start = time.Now().Nanosecond()
    fmt.Println(selfDividingNumbers2(1, 22))
    end = time.Now().Nanosecond()
    fmt.Println("time: ", (end - start)/1000)

}