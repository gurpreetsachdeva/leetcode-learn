package main

import "fmt"

func findComplement(num int) int {
    if num >= (1<<32) || num < 0{
        return -1
    }
    if num == 0 {
        return 1
    }
    bitCount := 1
    for i := num; i > 0; i /= 2 {
        bitCount *= 2
    }
    /*
        reference to other
    i := 0
    for ;i <= num; i <<= 1 {}
    */
    return num ^ (bitCount-1)
}

// reference to other, review binary number origin code and complement code
func findComplement2(num int) int {
    var mask = ^0
    for ; num&mask > 0; mask <<= 1 {}
    fmt.Println(mask)
    fmt.Println(^mask)
    return ^mask ^ num
}

func main() {
    //fmt.Println(findComplement(5))
    fmt.Println(findComplement2(5))

}
