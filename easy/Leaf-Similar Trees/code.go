package main


 // Definition for a binary tree node.
type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func preOrderTree(root *TreeNode) []int {
    if root == nil {
        return nil
    }
    if root.Left == nil && root.Right == nil {
        return []int{root.Val}
    }
    left := preOrderTree(root.Left)
    right := preOrderTree(root.Right)
    return append(left, right...)
}

func leafSimilar(root1 *TreeNode, root2 *TreeNode) bool {
    leafSeq1 := preOrderTree(root1)
    leafSeq2 := preOrderTree(root2)

    if len(leafSeq1) != len(leafSeq2) {
        return false
    }
    for i := range leafSeq1 {
        if leafSeq1[i] != leafSeq2[i] {
            return false
        }
    }
    return true
}


func main() {

}
