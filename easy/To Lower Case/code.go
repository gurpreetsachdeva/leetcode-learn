package main

import (
	"strings"
	"fmt"
)

func toLowerCase(str string) string {
	return strings.ToLower(str)
}


func toLowerCase2(str string) string {
	var n []uint8

	for i := range []uint8(str) {
		if str[i] >= 65 && str[i] <= 90 {
			n = append(n, str[i]+32)
		} else {
			n = append(n, str[i])
		}
	}
	return string(n)
}

// references to other
func toLowerCase3(str string) string {
	n := []rune(str)    // [72 69 76 76 79 32 87 79 82 76 68 32 20013 22269]
	//n := []byte(str)  // [72 69 76 76 79 32 87 79 82 76 68 32 228 184 173 229 155 189]
	//n := []uint8(str) // [72 69 76 76 79 32 87 79 82 76 68 32 228 184 173 229 155 189]
	//n := []int32(str) // [72 69 76 76 79 32 87 79 82 76 68 32 20013 22269]

	for i := range n {
		if n[i] >= 65 && n[i] <= 90 {
			n[i] += 32
		}
	}
	return string(n)
}


func main() {

	str := "HELLO WORLD 中国"
	fmt.Println(toLowerCase(str))
	fmt.Println(toLowerCase2(str))
	fmt.Println(toLowerCase3(str))

}
