package main

import (
	"strings"
	"fmt"
)

func judgeCircle(moves string) bool {

	coordinate := []int{0, 0}
	moves = strings.ToUpper(moves)
	for i := range moves {
		switch moves[i] {
		case 'U':
			coordinate[1]++
		case 'D':
			coordinate[1]--
		case 'L':
			coordinate[0]--
		case 'R':
			coordinate[0]++
		}
	}

	if coordinate[0] == 0 && coordinate[1] == 0 {
		return true
	}
	return false
}

func main() {
	fmt.Println(judgeCircle("ll"))
	fmt.Println(judgeCircle("uD"))
}
