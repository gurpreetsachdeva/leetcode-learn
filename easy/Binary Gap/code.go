package main

import "fmt"

func binaryGap(N int) int {
    binaryStr := fmt.Sprintf("%b", N)
    distance := 0
    pre := -1
    for i := range binaryStr {
        if binaryStr[i] == '1' {
            if pre == -1 {
                pre = i
            } else {
                if distance < i - pre {
                    distance = i - pre
                }
                pre = i
            }
        }
    }
    fmt.Println(distance)
    return distance
}

func main() {
    binaryGap(4)
}
