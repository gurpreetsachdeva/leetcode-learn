package main


// Definition for a binary tree node.
type TreeNode struct {
	Val int
	Left *TreeNode
	Right *TreeNode
}

func trimBST(root *TreeNode, L int, R int) *TreeNode {
	if root == nil {
		return nil
	}

	if root.Val >= L && root.Val <= R {
		if root.Left != nil {
			root.Left = trimBST(root.Left, L, R)
		}
		if root.Right != nil {
			root.Right = trimBST(root.Right, L, R)
		}
	}
	newRoot := root
	if left := trimBST(root.Left, L, R); left != nil {
		newRoot = left
	}
	if right := trimBST(root.Right, L, R); right != nil {
		newRoot = right
	}
	if newRoot == root {
		newRoot = nil
	}
	return newRoot
}

func main() {

}
