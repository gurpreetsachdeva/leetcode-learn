package main


func distributeCandies(candies []int) int {

	candiesMap := make(map[int]int)
	for i := range candies {
		candiesMap[candies[i]]++
	}

	if len(candies) / 2 >= len(candiesMap) {
		return len(candiesMap)
	}
	return len(candies) / 2
}

func main() {
	candies := []int{1,1,2,2,3,3}
	distributeCandies(candies)
}