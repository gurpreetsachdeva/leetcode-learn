package main

import "fmt"

func isToeplitzMatrix(matrix [][]int) bool {

	row := len(matrix)
	col := len(matrix[0])

	var output [][]int
	var left, right []int
	for j := 0; j < col; j++ {
		right = nil
		for i := 0; i < row && i+j < col; i++ {
			right = append(right, matrix[i][i+j])
		}
		if right != nil {
			output = append(output, right)
		}
	}

	for i := 1; i < row; i++ {
		left = nil
		for j := 0; j < col && i + j < row; j++ {
			left = append(left, matrix[i+j][j])
		}
		if left != nil {
			output = append(output, left)
		}
	}
	fmt.Println(output)
	return false
}

func isToeplitzMatrix2(matrix [][]int) bool {
	row := len(matrix)
	col := len(matrix[0])

	for j := 0; j < col; j++ {
		for i := 0; i < row && i+j < col; i++ {
			if matrix[i][i+j] != matrix[0][j] {
				return false
			}
		}
	}
	for i := 1; i < row; i++ {
		for j := 0; j < col && i + j < row; j++ {
			if matrix[i+j][j] != matrix[i][0] {
				return false
			}
		}
	}
	return true
}

/*
	reference to other, it is so nice
 */
func isToeplitzMatrix3(matrix [][]int) bool {

	row := len(matrix)
	col := len(matrix[0])

	for i := 0; i < row - 1; i++ {
		for j := 0; i < col - 1; j++ {
			if matrix[i][j] != matrix[i+1][j+1] {
				return false
			}
		}
	}
	return true
}

func main() {
	matrix := [][]int{
		{41,45},
		{81,41},
		{73,81},
		{47,73},
		{0,47},
		{79,76},
	}

	fmt.Println(isToeplitzMatrix2(matrix))
}
