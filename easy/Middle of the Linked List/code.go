package main

import "fmt"

/**
 * Definition for singly-linked list.
 */
type ListNode struct {
    Val int
    Next *ListNode
}

func middleNode(head *ListNode) *ListNode {
    var count = 0
    p := head
    for p != nil {
        count++
        p = p.Next
    }
    p = head
    for i := 1; i <= count/2; i++ {
        p = p.Next
    }
    return p
}

func initListNode(arr []int, head *ListNode) {
    pre := head
    head.Val = arr[0]
    for i := 1; i < len(arr); i++ {
        p := &ListNode{
            Val: arr[i],
            Next: nil,
        }
        pre.Next = p
        pre = pre.Next
    }
}

func displayListNode(head *ListNode) {
    for p := head; p != nil; p = p.Next {
        fmt.Printf("%v ", p.Val)
    }
    fmt.Println()
}


func main() {

    arr := []int{1, 2, 3, 4, 5}
    head := &ListNode{}
    initListNode(arr, head)
    displayListNode(head)

    midNode := middleNode(head)
    displayListNode(midNode)

}
